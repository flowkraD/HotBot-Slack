const SlackBot=require("slackbots");        //Slack Bot API
const randomPuppy=require("random-puppy");  //Reddit Random image URL Fetcher
const axios=require("axios");               //Process Json files

//Initializing the bot
const bot=new SlackBot({
    token:"",       //(OAuth) Bot User Access Token
    name:"HotBot"   //Bot name
});

//Message letting know bot is online
bot.on('start',()=>{
    console.log(bot.name+" is online");
});

//Logging errors
bot.on('error',(err)=>console.log(err));

//Checking for user invoking the bot
bot.on('message',(data)=>{
    
    if(data.type!="message"){
    return;
    }
    
    handleMess(data.text);
});

//Responding to user requests
function handleMess(message){
    if(message.includes(" tell a joke")){
        ProJoke();//Programming Joke
    }else
    if(message.includes(" pat me senpai")){
        headPat();//\r\headpats
    }else
    if(message.includes(" dankmeme")){
        dankMeme();//\r\headpats
    }
}

//Functions corresponding to each request

//Programming Joke Function - Unique Joke on Programming 
//Axios fetching data from online random Json file
function ProJoke(){
    
    //Returns a json with a random joke
    axios.get("https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_joke")
    .then(res=>{
        const l1=res.data.setup;//Question
        const l2=res.data.punchline;//Punchline
    
        //Added your favourite slack emote here. Leave blank if your bot's pfp is better
        const emote={
            
        }
        //Posting Message
        bot.postMessageToChannel('jokes',`${l1} - ${l2}`,emote);
    });
}

//\r\Headpats
//random-puppy API
function headPat(){
    
    //Requesting URL using random puppy API
    randomPuppy("headpats").then(url=>{
        const emote={
            icon_emoji:':sunglasses:'
        }
        //Posting the URL
        bot.postMessageToChannel('headpat',`${url}`,emote);
    });
}

//\r\dankmemes
//rndom-puppy API
function dankMeme(){
    
    //Requesting URL using random puppy API
    randomPuppy("dankmemes").then(url=>{
        const emote={
            icon_emoji:':sunglasses:'
        }
        //Posting the URL
        bot.postMessageToChannel('memes',`${url}`,emote);
    });
}

/*
Thanks to the integration of Slack with imgur, an image URL will preview the image in chat
This saves the hassle of dowloading the image and agian uploading to view
Thank you Slack
*/
